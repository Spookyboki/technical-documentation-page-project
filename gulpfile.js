'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var cssmin = require('gulp-cssmin');
var uglify = require('gulp-uglify');

gulp.task('sass', function(){
    return gulp.src(['./sass/main.scss'])
    .pipe(concat('style.scss'))
    .pipe(sass().on('error', sass.logError))
    .pipe(cssmin())
    .pipe(gulp.dest('./css'));
});

gulp.task('scripts', function(){
    return gulp.src('./js/*.js')
    .pipe(concat('script.js'))
    .pipe(uglify())
    .pipe(gulp.dest('./js'));
});

gulp.task('sass:watch', function(){
    gulp.watch('./sass/**/*.scss',['sass']);
});

gulp.task('scripts:watch', function(){
    gulp.watch('./js/**/*.js', ['scripts']);
});